# Skip - Nutrition

## Requirements

- Node.js 6 + NPM
- Yarn
  `npm i -g yarn`
- Expo
  `npm i -g exp`
- [Watchman](https://facebook.github.io/watchman/docs/install.html)
- Expo Client
  - [AppStore](https://itunes.apple.com/app/apple-store/id982107779?ct=www&mt=8)
  - [PlayStore](https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www)

## Installing

`yarn`

## Running

`yarn start`
