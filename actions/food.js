export const SET_FOOD = 'USER/SET_FOOD';
export const CLEAR_FOOD = 'USER/CLEAR_FOOD';

export function setFood(food) {
  return { type: SET_FOOD, food };
}

export function clearFood() {
  return { type: CLEAR_FOOD };
}
