import { createStore, combineReducers } from 'redux';

import food from '../reducers/food';

const reducer = combineReducers({
  food,
});

export const store = createStore(reducer);
