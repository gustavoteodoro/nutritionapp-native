import styled from 'styled-components';
import { Primary } from '../../bosons/colors';

export const HomeContainer = styled.View`
  width: 100%;
  min-height: 100%;
  background: ${Primary};
`;
