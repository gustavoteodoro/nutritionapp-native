import styled from 'styled-components';


export const HeaderInfoContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 30px 0;
`;

export const HeaderInfoTitle = styled.Text`
  font-weight: 600;
  color: white;
  font-size: 20px;
  margin-left: 20px;
`;
