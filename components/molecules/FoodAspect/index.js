import React, { Component } from 'react';
import { number, string } from 'prop-types';
import { FoodAspectContainer, FoodAspectValue, FoodAspectLabel } from './styles';

class FoodAspect extends Component {
  render() {
    const {
      value,
      label,
    } = this.props;
    return (
      <FoodAspectContainer>
        <FoodAspectValue>
          {value}
        </FoodAspectValue>
        <FoodAspectLabel>
          {label}
        </FoodAspectLabel>
      </FoodAspectContainer>
    );
  }
}

FoodAspect.propTypes = {
  value: number,
  label: string.isRequired,
};

FoodAspect.defaultProps = {
  value: 0,
};

export default FoodAspect;
