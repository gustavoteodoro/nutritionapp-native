import styled from 'styled-components';

import { Secondary } from '../../bosons/colors';

export const FoodAspectContainer = styled.View`
  display: flex;
  width: 33%;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: 20px 0;

`;

export const FoodAspectValue = styled.Text`
  color: ${Secondary};
  font-size: 25px;
  margin-bottom: 10px;
`;

export const FoodAspectLabel = styled.Text`
  font-size: 12px;
`;
