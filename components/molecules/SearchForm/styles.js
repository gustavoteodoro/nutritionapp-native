import styled from 'styled-components';

export const SearchFormContainer = styled.View`
  display: flex;
  flex-direction: row;
  position: relative;
  width: 100%;
  background: white;
  padding: 20px;
`;

export const FormError = styled.Text`
  position: absolute;
  bottom: -30px;
  left: 10px;
  font-size: 14px;
  color: white;
`;
