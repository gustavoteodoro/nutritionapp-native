import styled from 'styled-components';

export const HeaderContainer = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
