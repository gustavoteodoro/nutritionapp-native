import React, { Component } from 'react';
import HeaderInfo from '../../molecules/HeaderInfo';
import SearchForm from '../../molecules/SearchForm';
import { HeaderContainer } from './styles';

class Header extends Component {
  render() {
    return (
      <HeaderContainer>
        <HeaderInfo />
        <SearchForm />
      </HeaderContainer>
    );
  }
}

export default Header;
