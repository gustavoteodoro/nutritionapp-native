import styled from 'styled-components';

import { Primary } from '../../bosons/colors';

export const FoodInfoContainer = styled.View`
  display: flex;
  width: 100%;
  flex-wrap: wrap;
  flex-direction: row;
  background: white;
  box-shadow: 3px 3px 5px #0003;
`;

export const FoodName = styled.Text`
  color: ${Primary};
  font-size: 40px;
  font-weight: 600;
  margin-left: 20px;
`;

export const FoodInfoHeader = styled.View`
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;
`;
