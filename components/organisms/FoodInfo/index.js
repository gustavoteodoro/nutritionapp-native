import React, { Component } from 'react';
import {
  shape, number, string, object,
} from 'prop-types';
import { connect } from 'react-redux';
import FoodAspect from '../../molecules/FoodAspect';
import { clearFood } from '../../../actions/food';
import {
  FoodInfoContainer,
  FoodInfoHeader,
  FoodName,
} from './styles';

import {
  calories,
  cholesterol,
  dietaryFiber,
  potassium,
  protein,
  saturatedFat,
  sodium,
  sugars,
  totalCarbohydrate,
  totalFat,
} from './data.json';

class FoodInfo extends Component {
  render() {
    const {
      nf_calories,
      nf_cholesterol,
      nf_dietary_fiber,
      nf_potassium,
      nf_protein,
      nf_saturated_fat,
      nf_sodium,
      nf_sugars,
      nf_total_carbohydrate,
      nf_total_fat,
      food_name,
    } = this.props.food;
    return (
      <FoodInfoContainer>
        <FoodInfoHeader>
          <FoodName>
            {food_name}
          </FoodName>
        </FoodInfoHeader>
        <FoodAspect value={nf_calories} label={calories} />
        <FoodAspect value={nf_cholesterol} label={cholesterol} />
        <FoodAspect value={nf_dietary_fiber} label={dietaryFiber} />
        <FoodAspect value={nf_potassium} label={potassium} />
        <FoodAspect value={nf_protein} label={protein} />
        <FoodAspect value={nf_saturated_fat} label={saturatedFat} />
        <FoodAspect value={nf_sodium} label={sodium} />
        <FoodAspect value={nf_sugars} label={sugars} />
        <FoodAspect value={nf_total_carbohydrate} label={totalCarbohydrate} />
        <FoodAspect value={nf_total_fat} label={totalFat} />
      </FoodInfoContainer>
    );
  }
}

FoodInfo.propTypes = {
  food: shape({
    nf_calories: number,
    nf_cholesterol: number,
    nf_dietary_fiber: number,
    nf_potassium: number,
    nf_protein: number,
    nf_saturated_fat: number,
    nf_sodium: number,
    nf_sugars: number,
    nf_total_carbohydrate: number,
    nf_total_fat: number,
    photo: object,
    food_name: string,
  }),
};

FoodInfo.defaultProps = {
  food: shape({
    nf_calories: 0,
    nf_cholesterol: 0,
    nf_dietary_fiber: 0,
    nf_potassium: 0,
    nf_protein: 0,
    nf_saturated_fat: 0,
    nf_sodium: 0,
    nf_sugars: 0,
    nf_total_carbohydrate: 0,
    nf_total_fat: 0,
    photo: null,
    food_name: '',
  }),
};

const mapStateToProps = state => ({
  food: state.food,
});

const mapDispatchToProps = dispatch => ({
  clearFood() {
    dispatch(clearFood());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(FoodInfo);
