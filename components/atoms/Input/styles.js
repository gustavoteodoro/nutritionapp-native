import styled from 'styled-components';
import { Primary } from '../../bosons/colors';

export const InputElement = styled.TextInput`
  width: 80%;
  height: 40px;
  color: ${Primary};
  border: 2px solid ${Primary};
  font-size: 14px;
  padding: 10px;
`;
