import React, { Component } from 'react';
import { string } from 'prop-types';

import { InputElement } from './styles';

class Input extends Component {
  render() {
    const {
      label,
      value,
    } = this.props;

    return (
      <InputElement {...this.props} type="text" placeholder={label} value={value} autoCorrect={false} underlineColorAndroid="transparent" />
    );
  }
}

Input.propTypes = {
  label: string.isRequired,
  value: string,
};

Input.defaultProps = {
  value: '',
};

export default Input;
