import styled from 'styled-components';
import { Primary } from '../../bosons/colors';

export const ButtonInput = styled.TouchableOpacity`
  width: 20%;
  height: 40;
  background: ${Primary};
  padding: 10px;
`;

export const ButtonLabel = styled.Text`
  font-size: 14px;
  color: white;
`;
