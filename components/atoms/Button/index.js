import React, { Component } from 'react';
import { Image } from 'react-native';
import { string, bool } from 'prop-types';
import Loader from '../../../assets/img/loader.gif';

import { ButtonInput, ButtonLabel } from './styles';

class Button extends Component {
  render() {
    const {
      label,
      loading,
    } = this.props;

    return (
      <ButtonInput {...this.props}>
        <ButtonLabel>
          {label}
        </ButtonLabel>
      </ButtonInput>
    );
  }
}

Button.propTypes = {
  label: string.isRequired,
  loading: bool,
};

Button.defaultProps = {
  loading: false,
};

export default Button;
