import React, { Component } from 'react';
import SkipLogo from '../../../assets/img/logo.png';

import {
  LogoContent,
} from './styles';

class Logo extends Component {
  render() {
    return (
      <LogoContent source={SkipLogo} width="40" height="40" alt="Skip Logo" />
    );
  }
}

export default Logo;
