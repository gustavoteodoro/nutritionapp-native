import { API_URL, AppID, AppKey } from '../utils/constants';

export const getNutrients = (query) => fetch(
    `${API_URL}/natural/nutrients`,
    {
      headers: {
        'Content-Type': 'application/json',
        'x-app-id': AppID,
        'x-app-key': AppKey,
      },
      method: 'POST',
      body: JSON.stringify({
        query,
      }),
    },
  );
  